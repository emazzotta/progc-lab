/* ----------------------------------------------------------------------------
 * --  _____       ______  _____                                              -
 * -- |_   _|     |  ____|/ ____|                                             -
 * --   | |  _ __ | |__  | (___    Institute of Embedded Systems              -
 * --   | | | '_ \|  __|  \___ \   Zuercher Hochschule Winterthur             -
 * --  _| |_| | | | |____ ____) |  (University of Applied Sciences)           -
 * -- |_____|_| |_|______|_____/   8401 Winterthur, Switzerland               -
 * ----------------------------------------------------------------------------
 */
/**
 * @file
 * @brief Lab implementation
 */
#include <stdio.h>
#include <stdlib.h>
#include "fahrenheit_converter.h"

int main(int argc, char **argv)
{
    (void)printf("F'heit    Celsius\n");
    (void)printf("-----------------\n");
    for(int i = -100; i <= 200; i += 20) {
        (void)printf("  %4d    %6.2lf\n", i, fahrenheit_to_celsius(i));
    }
    return EXIT_SUCCESS;
}