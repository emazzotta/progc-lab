#include <stdio.h>

int word_counter(void)
{
    char read;
    int chars = 0;
    int words = 0;
    // if last char was a char, set it to 1
    // if it was a empty space, set it to 0
    // this prevents, that multiple spaces count as multiple words
    int last_char = 0;
    while(read != '\n') {
        read = getchar();
        if(isspace(read)){
            // only count as word, if last char was in fact a char and no whitespace
            if(last_char == 1) {
                words += 1;
                last_char = 0;
            }
        } else {
            chars += 1;
            last_char = 1;
        }
    }
    (void)printf("chars: %d words: %d", chars, words);
}