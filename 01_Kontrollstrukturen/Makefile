TARGET            := bin/program
TARGET_FULL_PATH  := $(CURDIR)/$(TARGET)
SOURCES           := $(wildcard src/*.c)
TEST_SOURCES      := tests/tests.c
CREATE_DIRS       := bin doc
OBJECTS           := $(SOURCES:%.c=%.o)
DEPENDENCIES      := $(SOURCES:%.c=%.d)
TEST_OBJECTS      := $(TEST_SOURCES:%.c=%.o)
TEST_DEPENDENCIES := $(TEST_SOURCES:%.c=%.d)
TEST_TARGET       := $(CURDIR)/tests/runtest
CUNIT_IN_CDIR     := $(CURDIR)/../CUnit/include
CUNIT_LIB_DIR     := $(CURDIR)/../CUnit/lib
TEST_IN_CDIR      := $(CURDIR)/../include
TEST_LIB_DIR      := $(CURDIR)/../lib
CC                = gcc
CFLAGS            = -std=c99 -Wall -g
CPPFLAGS          = -MD -Isrc -Itests -I$(TEST_IN_CDIR) -I$(CUNIT_IN_CDIR) -DTARGET=$(TARGET_FULL_PATH)
LDFLAGS           = -static -z muldefs

.PHONY: default clean test doc mkdir

default: $(TARGET_FULL_PATH)
	@echo "#### $< built ####"

$(TARGET_FULL_PATH): mkdir $(OBJECTS) Makefile
	$(LINK.c) -o $@ $(OBJECTS)

clean:
	$(RM) $(TARGET) $(OBJECTS) $(DEPENDENCIES) $(TEST_TARGET) $(TEST_OBJECTS) $(TEST_DEPENDENCIES) $(wildcard */*~ *~ tests/*.txt)
	$(RM) -r $(CREATE_DIRS)
	@echo "#### $@ done ####"

doc:
	doxygen ../Doxyfile > /dev/null
	@echo "#### $@ done ####"

test: $(TEST_TARGET)
	(cd tests; $(TEST_TARGET))
	@echo "#### $< executed ####"

$(TEST_TARGET): $(TARGET_FULL_PATH) $(TEST_OBJECTS)
	$(LINK.c) -o $(TEST_TARGET) $(TEST_OBJECTS) $(OBJECTS) -L$(CUNIT_LIB_DIR) -lcunit -L$(TEST_LIB_DIR) -lprogctest
	@echo "#### $@ built ####"

mkdir:
	-mkdir -p $(CREATE_DIRS)

-include $(DEPENDENCIES) $(TEST_DEPENDENCIES)
