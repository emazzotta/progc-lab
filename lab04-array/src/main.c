/* ----------------------------------------------------------------------------
 * --  _____       ______  _____                                              -
 * -- |_   _|     |  ____|/ ____|                                             -
 * --   | |  _ __ | |__  | (___    Institute of Embedded Systems              -
 * --   | | | '_ \|  __|  \___ \   Zuercher Hochschule Winterthur             -
 * --  _| |_| | | | |____ ____) |  (University of Applied Sciences)           -
 * -- |_____|_| |_|______|_____/   8401 Winterthur, Switzerland               -
 * ----------------------------------------------------------------------------
 */
/**
 * @file
 * @brief Lab implementation
 */
#include <stdio.h>
#include <stdlib.h>

#define ASCII_A 'A'
#define ASCII_1 '1'
#define BOARD_WIDTH 8
#define BOARD_HEIGHT 8

static void print_board(char board[BOARD_HEIGHT][BOARD_WIDTH][3])
{
	// begin students to add code for task 4.1

	// print board (each field with leading space, including first field of each row).
	for(int row=0;row<BOARD_HEIGHT;row++) {
	    for(int col=0;col<BOARD_WIDTH;col++) {
	        (void)printf(" %s", board[row][col]);
	    }
	    (void)printf("\n");
	}
	
	// end students to add code
}

/**
 * @brief Main entry point.
 * @returns Returns EXIT_SUCCESS (=0) on success.
 */
int main(void)
{
	// begin students to add code for task 4.1

	// define board variable (8x8 fields, each field has 3 chars)
    char board[BOARD_HEIGHT][BOARD_WIDTH][3];

	// initialize each field according to task assignment description
	for(int row=0;row<BOARD_HEIGHT;row++) {
        for(int col=0;col<BOARD_WIDTH;col++) {
            board[row][col][0] = ASCII_A + col;
            board[row][col][1] = ASCII_1 + row;
            board[row][col][2] = '\0';
        }
    }

	// call print_board(board);
	print_board(board);

	// end students to add code
	
	return EXIT_SUCCESS;
}
