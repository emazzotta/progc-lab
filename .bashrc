export LSCOLORS=GxFxBxDxCxegedabagacad
export LS_COLORS="di=1;36;40:ln=1;35;40:so=1;31;40:pi=1;33;40:ex=1;32;40:bd=34;46:cd=34;43:su=0;41:sg=0;46:tw=0;42:ow=0;43:"
export EDITOR=/usr/bin/vim
export CLICOLOR=1
export TERM="xterm-256color"
export PS1="\[\033[31m\]\t \[\033[32m\]\u@\h\[\033[00m\] in \[\033[33m\]\w\[\033[0m\]> "

alias t="make && make test && make doc"
alias vi='vim'
alias df='df -H'
alias du='du -ch'

alias ls='ls --color'
alias ll='ls -ltra'
alias l='clear'
alias e='exit'

alias .1="cd .."
alias .2=".1;.1"
alias .3=".2;.1"

alias ep='vi ~/.bashrc;rp'
alias rp='source ~/.bashrc'
alias tp='cd /tmp'
alias .s='cd ~/.ssh'

alias mkdir='mkdir -pv'
alias grep='grep --color=auto'
alias egrep='egrep --color=auto'
alias fgrep='fgrep --color=auto'
alias cp='cp -i'
alias mv='mv -i'
alias rm='rm -i'

a () {
	echo "###################################################"
	echo "#      _    _     ___    _    ____  _____ ____    #"
	echo "#     / \  | |   |_ _|  / \  / ___|| ____/ ___|   #"
	echo "#    / _ \ | |    | |  / _ \ \___ \|  _| \___ \   #"
	echo "#   / ___ \| |___ | | / ___ \ ___) | |___ ___) |  #"
	echo "#  /_/   \_\_____|___/_/   \_\____/|_____|____/   #"
	echo "#                                                 #"
	echo "###################################################"
	cat ~/.bashrc | grep --color=auto alias | awk '{$1="→ "; print $0}'
	cat ~/.bashrc | grep --color=auto function | awk '{$1="→→  "; print $1 $2}'
	echo "###################################################"
}


function agrep () {
    if [ ! -z ${1} ]
    then
        a | grep --color=auto ${1}
        declare -f ${1}
    else
        echo "Please pass an argument"
    fi
}