var indexSectionsWithContent =
{
  0: "lmopt",
  1: "lp",
  2: "mt",
  3: "m",
  4: "o",
  5: "p"
};

var indexSectionNames =
{
  0: "all",
  1: "classes",
  2: "files",
  3: "functions",
  4: "defines",
  5: "pages"
};

var indexSectionLabels =
{
  0: "All",
  1: "Data Structures",
  2: "Files",
  3: "Functions",
  4: "Macros",
  5: "Pages"
};

