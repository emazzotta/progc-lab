/* ----------------------------------------------------------------------------
 * --  _____       ______  _____                                              -
 * -- |_   _|     |  ____|/ ____|                                             -
 * --   | |  _ __ | |__  | (___    Institute of Embedded Systems              -
 * --   | | | '_ \|  __|  \___ \   Zuercher Hochschule Winterthur             -
 * --  _| |_| | | | |____ ____) |  (University of Applied Sciences)           -
 * -- |_____|_| |_|______|_____/   8401 Winterthur, Switzerland               -
 * ----------------------------------------------------------------------------
 */
/**
 * @file
 * @brief Lab implementation
 */
#include <stdio.h>
#include <stdlib.h>
#include <math.h>

/**
 * @brief  point structure of double coordinates
 */
// begin students to add code for task 4.2
struct Point2D {
    double x;
    double y;
};
// end students to add code

/**
 * line structure of two points
 */
// begin students to add code for task 4.2
struct Line {
    struct Point2D a;
    struct Point2D b;
};
// end students to add code

/**
 * @brief Main entry point.
 * @param[in] argc  The size of the argv array.
 * @param[in] argv  The command line arguments,
 *                  with argv[0] being the command call,
 *                  argv[1] the 1st argument,
 *                  argv[argc-1] the last argument.
 * @returns Returns EXIT_SUCCESS (=0) on success and
 *                  EXIT_FAILURE (=1) on error.
 */
int main(int argc, char* argv[])
{
    int success = 0;
    double p1x = 0.0;
    double p1y = 0.0;
    double p2x = 0.0;
    double p2y = 0.0;

    if(argc != 5) {
        return EXIT_FAILURE;
    }

    success += sscanf(argv[1], "%lf", &p1x);
    success += sscanf(argv[2], "%lf", &p1y);
    success += sscanf(argv[3], "%lf", &p2x);
    success += sscanf(argv[4], "%lf", &p2y);

    if(success != 4) {
        return EXIT_FAILURE;
    }

    struct Line line = {
        {p1x, p1y},
        {p2x, p2y}
    };

    (void)printf("line %g/%g-%g/%g\n", line.a.x, line.a.y, line.b.x, line.b.y);

    return EXIT_SUCCESS;
}
