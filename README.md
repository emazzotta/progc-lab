[![License](http://img.shields.io/:license-mit-blue.svg)](http://doge.mit-license.org)[![Developed By](https://img.shields.io/badge/developed%20with%20♥%20by-Emanuele-red.svg)](https://emanuelemazzotta.com/)

# Prog C

A simple docker container in which one can run tests and generate doxygen documentation.

## Setup

### Build

```sh
docker build -t ${USER}/$(basename ${PWD}) ${PWD}
```

### Run

```sh
docker run -d -P -v "${PWD}":/usr/src/$(basename ${PWD}) ${USER}/$(basename ${PWD})
```
