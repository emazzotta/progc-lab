---
Tags: C, ProgC, Zusammenfassung, Programmelemente, Funktionen
---

# PROGC

---

Aufgekommene Fragen:

- Gibt es in C Funktions-Overload, und wie verhält sich dieses in Bezug auf den Unterschied bei der Parameterangabe (void) und () -- Parameterchecking?

---

#### Kompilieren

```gcc -o object-file-name c-program-name.c```

Programm starten: ``` ./c-program-name```

---

**Beispiel Bit-Shifting:**

```C
// t = 01001011
// t << 3
// 01011000

/*
00010000
2 << 3  = 2 * 2^3
00000010 << 3 = 00010000 = 2^4 = 16
// 2^3 * (k * 2^1) = k * 2^4

// 1 << 2 = 1 * 2^2
// 2 << 2 = 2 * 2^2
*/
```

**Beispiel Char-To-Int:**

```C
#include <stdio.h>

int main() {  
  char mychar = '5';
  int mychar_as_int = mychar - '0';
  
  printf("%d", mychar_as_int);
}

// Simon -> '0' == 48
```

**Beispiel einer Funktion mit 2 Funktionenen als parameter.**

```C
 int main() {  
  // Funktion die als Parameter ein generisches Array und zwei Funktionen akzeptiert.
  void sort(void x[], int (*compare)(), int (*exchange)()) {
    (*compare)(); // Aufruf der Funktion die im parameter (*compare)() übergeben wurde
    (*exchange)();// Aufruf der Funktion die im parameter (*exchange)() übergeben wurde
    x[0] = '1';
  }
  sort(x[], fu, bla);
}
```

**Beispiel einer Funktion - copyStringToHeap:**

```C
#include <stdlib.h>
#include <string.h>

char* copyStringToHeap(const char *sourceString){
  int length;
  char *stringOnHeap;
  length = strlen(sourceString);
  stringOnHeap = (char *) malloc((length + 1) * sizeof(char));
  if (NULL == stringOnHeap){
    exit(1);
  }
  return strncpy(stringOnHeap,sourceString,length);
} 
```



---



## T01 - Programmelemente

#### Keywords

| Keyword    | Definition                               | Keyword  | Definition                               |
| :--------- | :--------------------------------------- | -------- | ---------------------------------------- |
| auto       | Defines a local variable as having a local lifetime. | break    | Passes control out of the compound statement. |
| case       |                                          | char     | 2 Byte                                   |
| const      | Makes variable value or pointer parameter unmodifiable. | continue | Passes control to the begining of the loop. (Skips the rest of the code in the loop) |
| default    |                                          | do       | Do-while loop                            |
| double     | data-type (range: 1e-999 to 9.999...e999 / precision: 16 significant bits) | else     |                                          |
| enum       | Defines a set of constants of type int   | extern   | Indicates that an identifier is defined elsewhere. |
| float      | data-type (range: 1e-999 to 9.999...e999 / precision: 16 significant bits) | for      | for loop                                 |
| goto       | Unconditionally transfer control: (``goto identifier``) | if       |                                          |
| inline     |                                          | int      | 4 Byte                                   |
| long       |                                          | register |                                          |
| restrict   |                                          | return   |                                          |
| short      |                                          | signed   |                                          |
| sizeof     |                                          | static   | modifier where a variable keeps its value |
| struct     |                                          | switch   |                                          |
| typedef    |                                          | union    | Struct with multiple datatypes.          |
| unsigned   |                                          | void     |                                          |
| while      |                                          | volatile | Value of a variable can change anytime   |
| _Bool      |                                          | _Complex |                                          |
| _imaginary |                                          |          |                                          |

#### Allgemeines

0 = false
alles ausser 0 = true

#### Code examples

##### Use of struct

```c
#include <stdio.h>
#include <math.h>

// Define struct with name point and variables x, y
typedef struct 
{
  int x;
  int y;
} point;

float calcDistanceBetween(point p1, point p2) {
  return sqrt(pow((p2.y - p1.y), 2) + pow((p2.x - p1.x), 2));
}

int main()
{
  // Initialize variables p1, p2 with data type point and values
  point p1 = {-1, 1};
  point p2 = {2, 5};
  
  float distance = calcDistanceBetween(p1, p2);
  (void)printf("Distance between p1 and p2 = %f", distance);
  
  return 1;
}
```

Use of enum

```c
#include <stdio.h>

enum weekdays { 
  montag = 1,   // = 1
  dienstag,     // = 2
  mittwoch,     // = 3
  donnerstag,   // = 4
  freitag,      // = 5
  samstag,      // = 6
  sonntag       // = 7
} myDay;        // Declare myDay as type enum weekdays

int main()
{
  myDay = montag;
  
  (void)printf("Dienstag: %d", myDay);
  
  return 1;
}
```

Use of Pointers

```c
#include <stdio.h>

int main()
{

  int zahl; // Variablen Deklaration
  int *ptr; // Pointer Deklaration
  zahl = 42; // Variablen Initialisierung
  ptr = &zahl; // Pointer zeigt auf die Speicheradresse der Zahl
  
  printf("Der Wert von 'zahl' ist %u\n", zahl);
  printf("Die Adresse von'zahl' ist %p\n", ptr);
  printf("Die Groesse von 'ptr' ist %lu\n", sizeof(ptr));
  
  printf("\n");
  
  printf("Der Wert von 'pointer' ist %d\n", *ptr);
  printf("Die Adresse von'pointer' ist %p\n", ptr);
  printf("Die Groesse von 'pointer' ist %lu\n", sizeof(ptr));
  
  
  printf("\n");
  int zahl2 = 1337;
  //int* ptr2 = (int*)0x7ffd5c9dee34;
  int *ptr2 = &zahl;
  *ptr2 = zahl2;
  
  printf("Der Wert von 'pointer2' ist %d\n", *ptr2);
  printf("Die Adresse von'pointer2' ist %p\n", ptr2);
  printf("Die Groesse von 'pointer2' ist %lu\n", sizeof(ptr2));
  
  printf("\n");
  
  printf("Der Wert von 'pointer' ist %d\n", *ptr);
  printf("Die Adresse von'pointer' ist %p\n", ptr);
  printf("Die Groesse von 'pointer' ist %lu\n", sizeof(ptr));
  printf("Der Wert von 'zahl' ist %u\n", zahl);

  return 1;
}

//--------------------------------------------------------------------------------

#include <stdio.h>

int main()
{
  int zahl; // Variablen Deklaration
  int *ptr; // Pointer Deklaration
  zahl = 42; // Variablen Initialisierung
  ptr = &zahl; // Pointer zeigt auf die Speicheradresse der Zahl
  
  printf("Der Wert von 'zahl' ist %u\n", zahl);
  printf("Die Adresse von'zahl' ist %p\n", ptr);
  printf("Die Groesse von 'zahl' ist %lu\n", sizeof(zahl));
  printf("Die Groesse von 'ptr' ist %lu\n", sizeof(ptr));
  printf("Der Wert von 'ptr' ist %d\n", *ptr);
  printf("Die Adresse von'ptr' ist %p\n", ptr);
  
  return 1;
}
```



```c
#include <stdio.h>

int main(int argc, char* argv[])
{
  
  int foo = 0x23; // Hex 
  int bar = 023; // Oktal
  int baz = 23; // Dezimal
  
  printf("foo: %d bar: %d baz: %d", foo, bar, baz);


  return 0;
}
```



### Deklarationen und Definitionen

**Deklaration:**

- Eine Deklaration bestimmt wie ein Name verwendet werden kann.

**Definition:**

- Eine Variablen-Definition alloziert Speicher.
- Eine Funktions-Definition gitbt den Funktionsbody an.
- Eine Definition ist immer auch eine Deklaration.

**Typ Definition/Deklaration:**

- Basis Typen wie int, char, etc. sind implizit bekannt.
- User definierte Typen (struct und enum) mit Body definieren einen Typen.
- User definierte Typen ohne Body deklarieren den Typen nur.

**Typ Alias:**

- Für einen Typen kann mittels typedef ein Alias deklariert werden.
- Ein Alias definiert keinen neuen Typen - Alias und original Typ sind austauschbar.



### Input/Output

- Input/Output Deklarationen werden mit ```#include <stdio.h>``` bekannt gemacht.
- Funktionen für den einfachen Input/Output:
  - ```a = puts("Hello");``` // Ausgabe des Strings auf Standard Output
  - ```a = putchar('A');``` // Ausgabe des Zeichens auf Standard Output
  - ```c = getchar(void);``` // Einlesen eines Zeichens von Standard Input
- Funktionen für formatierte Ausgabe:
  - Generell: ``a = printf(format-string, arg1, arg2, ...); ``
  - ``(void)printf("Hello World\n");`` // Ohne weitere Argumente
  - `` (void)printf("The sum of %d abd %d is %d\n", a, b, a+b);`` // Mit Argumenten


- Konvertierungsoperatoren:
  - `` %d, %i`` - int; ``%u `` - unsigned int
  - `` %c`` - char
  - `` %s `` - char* (Zeichen des Strings werden ausgegeben bis \0 gefunden wird)
  - `` %f `` - double, float
  - Bei Zahlen können zwischen dem % und dem Konvertierungszeichen die minimale Länge **m** (anz. Zeichen) des Outputs und die Anzahl Dezimalstellen **d** angegeben werden: ``%m.df``


- Funktionen für formatierte Eingabe:
  - Generell: ``a = scanf(format-string, arg1, arg2, ...);``
  - Beispiel: Einlesen von 3 Werten aus Standard Input in die 3 *int* Variablen *day, month* und *year*:
    - ``a = scanf("%d%d%d", &day, &month, &year);`` 
    - **Achtung:** der Adressoperator & ist wichtig
    - Man beachte: auch das letzte Return-Zeichen (\n) wird in den Buffer eingelesen
      -> gegebenenfalls mit einem einzelnen ``getchar()`` auslesen.




**Codebeispiele:**

```C
#include <stdio.h>

int main(int argc, char* argv[])
{
  
  int foo = 0x23; // Hex 
  int bar = 023; // Oktal
  int baz = 23; // Dezimal
  
  double r = 3/2;    // 1.5000 f
  int i = 3.1415;    // 3
  int j = 2.99 + 3;  // 5
  
  
  (void)printf("foo: %d bar: %d baz: %d\n", foo, bar, baz);
  (void)printf("r: %lf i: %d j: %d",r,i,j);

  return 0;
}
```

*Ausgabe:*

> foo: 35 bar: 19 baz: 23 
>
> r: 1.000000 i: 3 j: 5 

---



## T02 - Funktionen 1



**Declared-Before-Used:**

- Jeder Name (Typ, Variable, Funktion, \#define) muss deklariert sein befor er verwendet wrd.
- z.B. printf, message und EXIT_SUCCESS müssen deklariert und gegebenenfalls definiert werden bevor sie verwendet werden können.
- Mögliche Lösungen:
  - Explizite Deklaration im File wo die Namen verwendet werden.	

    ```C
    int prinft(format, ..);
    \#define EXIT_SUCCESS 0
    const char *message = "Hello World!\n";
    ```

  - \#include von entsprechenden Header Files welche eine passende Deklaration enthalten

    ```C
     #include <stdio.h>
    ```





**One-Definition-Rule (ODR)**

- Jeder Name darf nur eine Definition im gesamten Programm haben.

  - z.B. eine Funktion darf beliebig oft (konsistent) **deklariert** werden, aber nur an einer Stelle **definiert** werden.

- Konsistente Deklarationen und Definitionen

  - Um Konsistenz zu erreichen ist es angebracht, öffentliche Deklarationen in Header Files unterzubringen. Jedes File welche eine entsprechende Deklaration benötigt, soll das Header file \#includen und die Deklaration nicht selber angeben.
  - Da ein Header File potentiell in verschiedenen Files eingebunden wird, darf ein Header File keine Funktiondefinitionen noch Variablendefinition enthalten.

- Ausnahmen zu ODR

  - Dieselbe Typedefinition darf in verschiedenen Source Files vorkommen, muss aber identisch sein.
  - Inline Funktionen dürfen mehrfach identisch vorkommen.

  ```C
  // In einem Header File:
  ...
  inline int max_int(int a, int b) { return a < b ? b : a; }
  ```

  ​

**Definition und Deklaration**

- Bei der Funktionsdefinition wird die eigentliche Funktion genau einmal im Programm implementiert **(ODR)**
- Zusätzlich wird die Funktionsdeklaration vor dem ersten Aufruf der Funktion im Programm benötigt **(DBU)**



**Beispiel Aufruf einer Funktion mit variabler Paramterlänge:**

``` C
#include <stdio.h>
#include <stdarg.h>
...
int var_func(int paramcount, ...) 
{
  int lv = 0;
  va_list lpos;
  
  for(va_start(lpos, paramcount); va_arg(lpos, int) != 0; lv++)
  {
  	printf("lv: %d", lv);
  }
  va_end(lpos);
  return lv;
};
...
int z1 = 3;
double z2 = 5.4;
...
  
var_func(z1, z2, 0); /* 1 zusätzlicher String */
var_func(z1, z2, 19, 0); /* 2 zusätzliche Integerwerte */
...
```


**Parameter und Rückgabewerte**

- Folgende Typen sind als *Parameter* zugelassen:
  - Basis Datentypen
  - Strukturen
  - Arrays
  - Pointer
- Folgende Typen sind als *Rückgabewerte* zugelassen:
  - Basis Datentypen
  - Strukturen
  - Pointer
- In C werden Parameter immer *"by value"* übergeben, d.h. die Werte der Variablen, die der Funktion übergeben werden, werden in die Funktion *"hinein kopiert"*
- Die Parameter können dann innerhalb der Funktion wie *"normale"* lokale Variablen verwendet werden; ebenfalls kann deren Wert verändert werden



**Lokale / Globale / Statische Variablen**

> - Sichtbarkeit von Variablen bzw. lokale Variablen verhalten sich gleich wie in Java.
> - Globale Variablen sind im gesamten Programm zugreifbar und werden implizit mit 0 initialisiert.
> - Statische Variablen verhalten sich genau gleich wie globale Variablen, ausser dass sie nur innerhalb der gegebenen Quelldatei sichtbar sind. (Auch Funktionen können als static deklariert und definiert werden)
>   - Der Effekt von statischen Variablen ist, dass der Wert der Variable den Aufruf der Funktion überlebt, d.h. beim nächsten Aufruf ist der vorherige Wert vorhanden.