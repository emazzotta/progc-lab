/* ----------------------------------------------------------------------------
 * --  _____       ______  _____                                              -
 * -- |_   _|     |  ____|/ ____|                                             -
 * --   | |  _ __ | |__  | (___    Institute of Embedded Systems              -
 * --   | | | '_ \|  __|  \___ \   Zuercher Hochschule Winterthur             -
 * --  _| |_| | | | |____ ____) |  (University of Applied Sciences)           -
 * -- |_____|_| |_|______|_____/   8401 Winterthur, Switzerland               -
 * ----------------------------------------------------------------------------
 */
/**
 * @file
 * @brief Lab implementation
 */
#include <stdio.h>
#include <stdlib.h>

/**
 * @brief Main entry point.
 * @param[in] argc  The size of the argv array.
 * @param[in] argv  The command line arguments
 *                  with argv[0] being the command call
 *                  argv[1] the 1st argument, ...
 *                  argv[argc-1] the last argument.
 * @returns Returns the rest of the calculation or
 *                  255 on failure
 */
int main(int argc, char* argv[])
{
	int rappen = 0;

	if (argc != 2 || sscanf(argv[1], "%d", &rappen) != 1 || rappen < 0) {
	    (void)printf("Bitte gültigen Rappenbetrag eingeben.");
	    return 255;
	}

    (void)printf("CHF %.2f:\n", (double)rappen/100);

	int aufteilung[] = {500, 200, 100, 50, 20, 10, 5};
	for(int i=0;i<sizeof(aufteilung)/sizeof(int);i++) {
	    int passt_x_mal_in_rappen = 0;
	    while(rappen - aufteilung[i] >= 0) {
	        rappen -= aufteilung[i];
	        passt_x_mal_in_rappen++;
	    }
        (void)printf("- %d x %.2f\n", passt_x_mal_in_rappen, (double)aufteilung[i]/100);
	}

    if(rappen > 0) {
        (void)printf("Rest = %.2f\n", (double)rappen/100);
    } else {
        (void)printf("Kein Rest\n");
    }


	return rappen; // rest = 0 = success
}
