/* ----------------------------------------------------------------------------
 * --  _____       ______  _____                                              -
 * -- |_   _|     |  ____|/ ____|                                             -
 * --   | |  _ __ | |__  | (___    Institute of Embedded Systems              -
 * --   | | | '_ \|  __|  \___ \   Zuercher Hochschule Winterthur             -
 * --  _| |_| | | | |____ ____) |  (University of Applied Sciences)           -
 * -- |_____|_| |_|______|_____/   8401 Winterthur, Switzerland               -
 * ----------------------------------------------------------------------------
 */
/**
 * @file
 * @brief Lab implementation
 */
#include <stdio.h>
#include <stdlib.h>
#include <math.h>

/**
 * @brief  point structure of double coordinates
 */
// begin students to add code for task 4.1
struct Point2D {
    double x;
    double y;
};
// end students to add code


/**
 * @brief Main entry point.
 * @param[in] argc  The size of the argv array.
 * @param[in] argv  The command line arguments,
 *                  with argv[0] being the command call,
 *                  argv[1] the 1st argument,
 *                  argv[argc-1] the last argument.
 * @returns Returns EXIT_SUCCESS (=0) on success.
 */
int main(int argc, char* argv[])
{
	double distance = 0.0;
    int success = 0;
    struct Point2D p1;
    struct Point2D p2;

    if(argc != 5) {
        return EXIT_FAILURE;
    }

    success += sscanf(argv[1], "%lf", &p1.x);
    success += sscanf(argv[2], "%lf", &p1.y);
    success += sscanf(argv[3], "%lf", &p2.x);
    success += sscanf(argv[4], "%lf", &p2.y);

    if(success != 4) {
        return EXIT_FAILURE;
    }

    double dx = p1.x - p2.x;
    double dy = p1.y - p2.y;
    distance = sqrt(dx*dx+dy*dy);

	(void)printf("distance = %g\n", distance);
	
	return EXIT_SUCCESS;
}
