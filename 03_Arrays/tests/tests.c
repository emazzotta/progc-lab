/* ----------------------------------------------------------------------------
 * --  _____       ______  _____                                              -
 * -- |_   _|     |  ____|/ ____|                                             -
 * --   | |  _ __ | |__  | (___    Institute of Embedded Systems              -
 * --   | | | '_ \|  __|  \___ \   Zuercher Hochschule Winterthur             -
 * --  _| |_| | | | |____ ____) |  (University of Applied Sciences)           -
 * -- |_____|_| |_|______|_____/   8401 Winterthur, Switzerland               -
 * ----------------------------------------------------------------------------
 */
/**
 * @file
 * @brief Test suite for the given package.
 */
#include <stdio.h>
#include <stdlib.h>
#include "CUnit/Basic.h"
#include "test_utils.h"
#include "mark_statistics_calculator.h"

#ifndef TARGET
#error missing TARGET define
#endif

/// @brief The name of the STDOUT text file.
#define OUTFILE "stdout.txt"
/// @brief The name of the STDERR text file.
#define ERRFILE "stderr.txt"
/// @brief The name of the test input file with mark calculation data
#define INFILE "marks.test_txt"

// setup & cleanup
static int setup(void)
{
	remove_file_if_exists(OUTFILE);
	remove_file_if_exists(ERRFILE);
	return 0; // success
}

static int teardown(void)
{
	return 0; // success
}

// tests
static void test_acceptance(void)
{
	// arrange
    char *out_txt[] = {
    "Please enter the points of student 1: ",
    "Please enter the points of student 2: ",
    "Please enter the points of student 3: ",
    "Please enter the points of student 4: ",
    "Please enter the points of student 5: ",
    "Please enter the points of student 6: ",
    "Please enter the points of student 7: ",
    "Please enter the points of student 8: ",
    "Please enter the points of student 9: ",
    "Please enter the points of student 10: ",
    "Please enter the points of student 11: ",
    "Please enter the points needed for mark 6: ",
    "--------------------------------------------------------\n",
    "Statistics (10 students, 100 points needed for mark 6):\n",
    "	Mark 6: 3\n",
    "	Mark 5: 1\n",
    "	Mark 4: 2\n",
    "	Mark 3: 1\n",
    "	Mark 2: 3\n",
    "	Mark 1: 0\n",
    "	Best mark:    6\n",
    "	Worst mark:   2\n",
    "	Average mark: 4.00\n",
    "	Mark >= 4:    6 students (60.00%)\n",
    "--------------------------------------------------------\n",
    "Would you like to:\n",
    "1: Exit\n",
    "2: Rerun with different points needed for mark 6\n",
    "Please select (1/2): \n",
    };
    const char *err_txt[] = {};
    // act
    int exit_code = system(XSTR(TARGET) " 1>" OUTFILE " 2>" ERRFILE " <" INFILE);
    // assert
    CU_ASSERT_EQUAL(exit_code, 0);
    assert_lines(OUTFILE, out_txt, sizeof(out_txt)/sizeof(*out_txt));
}

/**
 * @brief Registers and runs the tests.
 */
int main(void)
{
	// setup, run, teardown
	TestMainBasic("Mark Statistic", setup, teardown
				  , test_acceptance
				  );
}