/* ----------------------------------------------------------------------------
 * --  _____       ______  _____                                              -
 * -- |_   _|     |  ____|/ ____|                                             -
 * --   | |  _ __ | |__  | (___    Institute of Embedded Systems              -
 * --   | | | '_ \|  __|  \___ \   Zuercher Hochschule Winterthur             -
 * --  _| |_| | | | |____ ____) |  (University of Applied Sciences)           -
 * -- |_____|_| |_|______|_____/   8401 Winterthur, Switzerland               -
 * ----------------------------------------------------------------------------
 */
/**
 * @file
 * @brief Lab implementation
 */
#include <stdio.h>
#include <stdlib.h>
#include "mark_statistics_calculator.h"

int main(void)
{
    Student students[100];
    int max_points = 0;
    int students_len = 0;
    int choice = 0;

    for(int i = 0; i < 100; i++) {
        printf("Please enter the points of student %d: ", (i+1));
        scanf("%d", &students[i].points);
        if(students[i].points == -1) {
            students_len = i;
            (void)printf("Please enter the points needed for mark 6: ");
            scanf("%d", &max_points);
            break;
        } else if(students[i].points < -1) {
            (void)printf("Negative points are not allowed! Try again:\n");
            i--;
        }
    }

    do {
        calculate_marks(students, students_len, max_points);
        print_statistics(students, students_len, max_points);
        (void)printf("Would you like to:\n");
        (void)printf("1: Exit\n");
        (void)printf("2: Rerun with different points needed for mark 6\n");
        do {
            (void)printf("Please select (1/2): \n");
            scanf("%d", &choice);
        } while(choice != 1 && choice != 2);
        if(choice == 2) {
            (void)printf("Please enter the new maximum points needed for mark 6: \n");
            scanf("%d", &max_points);
        }
	} while(choice != 1);
	return 0;
}

void calculate_marks(Student students[], size_t len, int max_points)
{
    for(int i = 0; i < len; i++) {
        students[i].mark = calculate_mark(students[i].points, max_points);
    }
}

int get_students_with_mark(Student students[], size_t len, int mark)
{
    int students_with_specific_mark = 0;
    for(int i = 0; i < len; i++) {
        if(students[i].mark == mark) {
            students_with_specific_mark++;
        }
    }
    return students_with_specific_mark;
}

int get_students_with_mark_over(Student students[], size_t len, int mark)
{
    int students_with_mark_over_threshold = 0;
    for(int i=0;i<len;i++) {
        if(students[i].mark >= mark) {
            students_with_mark_over_threshold++;
        }
    }
    return students_with_mark_over_threshold;
}

int get_best_mark(Student students[], size_t len)
{
    int best = 0;
    for(int i = 0; i < len; i++) {
        if(students[i].mark > best) {
            best = students[i].mark;
        }
    }
    return best;
}

int get_worst_mark(Student students[], size_t len)
{
    int worst = 6;
    for(int i = 0; i < len; i++) {
        if(students[i].mark < worst) {
            worst = students[i].mark;
        }
    }
    return worst;
}

double get_average_mark(Student students[], size_t len)
{
    float average = 0.0;
    for(int i = 0; i < len; i++) {
        average += students[i].mark;
    }
    return average/len;
}

double get_percentage(int dividend, int divisor)
{
    return (100.00 / dividend) * divisor;
}

int calculate_mark(int scored_points, int max_points)
{
    int mark = (1 + ((5.00 * scored_points) / max_points)) + 0.5;
    return mark > 6 ? 6 : mark;
}

void print_statistics(Student students[], size_t len, int max_points)
{
    (void)printf("--------------------------------------------------------\n");
    (void)printf("Statistics (%zu students, %d points needed for mark 6):\n", len, max_points);
    (void)printf("\tMark 6: %d\n", get_students_with_mark(students, len, 6));
    (void)printf("\tMark 5: %d\n", get_students_with_mark(students, len, 5));
    (void)printf("\tMark 4: %d\n", get_students_with_mark(students, len, 4));
    (void)printf("\tMark 3: %d\n", get_students_with_mark(students, len, 3));
    (void)printf("\tMark 2: %d\n", get_students_with_mark(students, len, 2));
    (void)printf("\tMark 1: %d\n", get_students_with_mark(students, len, 1));
    (void)printf("\tBest mark:    %d\n", get_best_mark(students, len));
    (void)printf("\tWorst mark:   %d\n", get_worst_mark(students, len));
    (void)printf("\tAverage mark: %.2f\n", get_average_mark(students, len));
    (void)printf("\tMark >= 4:    %d students (%.2f%%)\n", get_students_with_mark_over(students, len, 4), get_percentage(len, get_students_with_mark_over(students, len, 4)));
    (void)printf("--------------------------------------------------------\n");
}
