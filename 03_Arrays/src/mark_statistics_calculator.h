/* Header file main.h */
#ifndef MAIN_H
#define MAIN_H

typedef struct {
    int points;
    int mark;
} Student;

void calculate_marks(Student[], size_t len, int max_points);
int get_students_with_mark(Student[], size_t, int);
int get_students_with_mark_over(Student[], size_t, int);
int get_best_mark(Student[], size_t);
int get_worst_mark(Student[], size_t);
double get_average_mark(Student[], size_t);
double get_percentage(int, int);
int calculate_mark(int, int);
void print_statistics(Student[], size_t, int);

#endif
