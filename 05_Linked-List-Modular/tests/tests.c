/* ----------------------------------------------------------------------------
 * --  _____       ______  _____                                              -
 * -- |_   _|     |  ____|/ ____|                                             -
 * --   | |  _ __ | |__  | (___    Institute of Embedded Systems              -
 * --   | | | '_ \|  __|  \___ \   Zuercher Hochschule Winterthur             -
 * --  _| |_| | | | |____ ____) |  (University of Applied Sciences)           -
 * -- |_____|_| |_|______|_____/   8401 Winterthur, Switzerland               -
 * ----------------------------------------------------------------------------
 */
/**
 * @file
 * @brief Test suite for the given package.
 */
#include <stdio.h>
#include <stdlib.h>
#include "CUnit/Basic.h"
#include "test_utils.h"
#include "person_list.h"

#ifndef TARGET
#error missing TARGET define
#endif

/// @brief The name of the STDOUT text file.
#define OUTFILE "stdout.txt"
/// @brief The name of the STDERR text file.
#define ERRFILE "stderr.txt"
/// @brief The name of the test input file.
#define INFILE "input.test_txt"

// setup & cleanup
static int setup(void)
{
	remove_file_if_exists(OUTFILE);
	remove_file_if_exists(ERRFILE);
	return 0; // success
}

static int teardown(void)
{
	return 0; // success
}


// tests
static void test_acceptance(void)
{
	// arrange
    const char *out_txt[] = {
        "Please select action:\n",
        "I: Insert person\n",
        "R: Remove person\n",
        "S: Show list of all persons\n",
        "C: Clear all persons\n",
        "E: Exit\n",
        "\n",
        "Input:\n",
        "--- [Inserting] ---\n",
        "Please enter the name: Please enter the firstname: Please enter the age: Please select action:\n",
        "I: Insert person\n",
        "R: Remove person\n",
        "S: Show list of all persons\n",
        "C: Clear all persons\n",
        "E: Exit\n",
        "\n",
        "Input:\n",
        "Please select action:\n",
        "I: Insert person\n",
        "R: Remove person\n",
        "S: Show list of all persons\n",
        "C: Clear all persons\n",
        "E: Exit\n",
        "\n",
        "Input:\n",
        "--- [Inserting] ---\n",
        "Please enter the name: Please enter the firstname: Please enter the age: Please select action:\n",
        "I: Insert person\n",
        "R: Remove person\n",
        "S: Show list of all persons\n",
        "C: Clear all persons\n",
        "E: Exit\n",
        "\n",
        "Input:\n",
        "Please select action:\n",
        "I: Insert person\n",
        "R: Remove person\n",
        "S: Show list of all persons\n",
        "C: Clear all persons\n",
        "E: Exit\n",
        "\n",
        "Input:\n",
        "--- [Inserting] ---\n",
        "Please enter the name: Please enter the firstname: Please enter the age: Please select action:\n",
        "I: Insert person\n",
        "R: Remove person\n",
        "S: Show list of all persons\n",
        "C: Clear all persons\n",
        "E: Exit\n",
        "\n",
        "Input:\n",
        "Please select action:\n",
        "I: Insert person\n",
        "R: Remove person\n",
        "S: Show list of all persons\n",
        "C: Clear all persons\n",
        "E: Exit\n",
        "\n",
        "Input:\n",
        "--- [Showing] ---\n",
        "Person 1\n",
        "	Name: mazzotta\n",
        "	Firstname: emanuele\n",
        "	Age: 22\n",
        "Person 2\n",
        "	Name: kuster\n",
        "	Firstname: leandro\n",
        "	Age: 23\n",
        "Person 3\n",
        "	Name: breiter\n",
        "	Firstname: simon\n",
        "	Age: 28\n",
        "Press any key to continue...Please select action:\n",
        "I: Insert person\n",
        "R: Remove person\n",
        "S: Show list of all persons\n",
        "C: Clear all persons\n",
        "E: Exit\n",
        "\n",
        "Input:\n",
        "--- [Removing] ---\n",
        "Please enter the name: Please enter the firstname: Please enter the age: Successfully removed breiter simon!\n",
        "\n",
        "Press any key to continue...Please select action:\n",
        "I: Insert person\n",
        "R: Remove person\n",
        "S: Show list of all persons\n",
        "C: Clear all persons\n",
        "E: Exit\n",
        "\n",
        "Input:\n",
        "--- [Showing] ---\n",
        "Person 1\n",
        "	Name: mazzotta\n",
        "	Firstname: emanuele\n",
        "	Age: 22\n",
        "Person 2\n",
        "	Name: kuster\n",
        "	Firstname: leandro\n",
        "	Age: 23\n",
        "Press any key to continue...Please select action:\n",
        "I: Insert person\n",
        "R: Remove person\n",
        "S: Show list of all persons\n",
        "C: Clear all persons\n",
        "E: Exit\n",
        "\n",
        "Input:\n",
        "--- [Clearing] ---\n",
        "Successfully cleared list!\n",
        "\n",
        "Press any key to continue...Please select action:\n",
        "I: Insert person\n",
        "R: Remove person\n",
        "S: Show list of all persons\n",
        "C: Clear all persons\n",
        "E: Exit\n",
        "\n",
        "Input:\n",
        "--- [Showing] ---\n",
        "Press any key to continue...Please select action:\n",
        "I: Insert person\n",
        "R: Remove person\n",
        "S: Show list of all persons\n",
        "C: Clear all persons\n",
        "E: Exit\n",
        "\n",
        "Input:\n",
    };
    // act
    int exit_code = system(XSTR(TARGET) " 1>" OUTFILE " 2>" ERRFILE " <" INFILE);
    // assert
    CU_ASSERT_EQUAL(exit_code, 0);
    assert_lines(OUTFILE, out_txt, sizeof(out_txt)/sizeof(*out_txt));
}

/**
 * @brief Registers and runs the tests.
 */
int main(void)
{
	// setup, run, teardown
	TestMainBasic("Person List", setup, teardown
				  , test_acceptance
				  );
}
