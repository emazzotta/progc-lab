/* ----------------------------------------------------------------------------
 * --  _____       ______  _____                                              -
 * -- |_   _|     |  ____|/ ____|                                             -
 * --   | |  _ __ | |__  | (___    Institute of Embedded Systems              -
 * --   | | | '_ \|  __|  \___ \   Zuercher Hochschule Winterthur             -
 * --  _| |_| | | | |____ ____) |  (University of Applied Sciences)           -
 * -- |_____|_| |_|______|_____/   8401 Winterthur, Switzerland               -
 * ----------------------------------------------------------------------------
 */
/**
 * @file
 * @brief Lab implementation
 */
#include <stdio.h>
#include <stdlib.h>
#include <ctype.h>
#include <string.h>
#include "person_list.h"

int main(void)
{
    char choice = 'X';
    ListElement *persons;
    persons = malloc(sizeof(ListElement));
    persons->next = persons;

	do {
	    // system("clear");
	    (void)printf("Please select action:\n");
        (void)printf("I: Insert person\n");
        (void)printf("R: Remove person\n");
        (void)printf("S: Show list of all persons\n");
        (void)printf("C: Clear all persons\n");
        (void)printf("E: Exit\n");
        (void)printf("\nInput:\n");
        scanf("%c", &choice);
        choice = toupper(choice);
        switch(choice) {
            case 'I': insert_person(persons); break;
            case 'R': remove_person(persons); break;
            case 'S': show_persons(persons); break;
            case 'C': clear_persons(persons); break;
        }
	} while(choice != 'E');
	return EXIT_SUCCESS;
}

void insert_person(ListElement *persons)
{
    (void)printf("--- [Inserting] ---\n");
    ListElement *start = persons;

    Person new_person;
    (void)printf("Please enter the name: ");
    scanf("%s", new_person.name);
    (void)printf("Please enter the firstname: ");
    scanf("%s", new_person.firstname);
    (void)printf("Please enter the age: ");
    scanf("%u", &new_person.age);

    while(persons->next!=start) {
        persons = persons->next;
    }

    persons->next = malloc(sizeof(ListElement));
    persons = persons->next;
    persons->content = new_person;
    persons->next = start;
    return;
}

void remove_person(ListElement *persons)
{
    (void)printf("--- [Removing] ---\n");
    Person to_remove;
    (void)printf("Please enter the name: ");
    scanf("%s", to_remove.name);
    (void)printf("Please enter the firstname: ");
    scanf("%s", to_remove.firstname);
    (void)printf("Please enter the age: ");
    scanf("%u", &to_remove.age);

    ListElement *start = persons;
    while(persons->next != start && personcmp((persons->next)->content, to_remove) != 0) {
        persons = persons->next;
    }
    if(persons->next != start) {
        ListElement *temp;
        temp = persons->next;
        persons->next = temp->next;
        free(temp);
        (void)printf("Successfully removed %s %s!\n\n", to_remove.name, to_remove.firstname);
    } else {
        (void)printf("Couldn't remove %s %s (Not found).\n\n", to_remove.name, to_remove.firstname);
    }
    pause_for_input();
}

void show_persons(ListElement *persons)
{
    (void)printf("--- [Showing] ---\n");
    ListElement *start = persons;
    int i = 1;
    while(persons->next!=start) {
        persons = persons->next;
        printf("Person %d\n\tName: %s\n\tFirstname: %s\n\tAge: %u\n", i, persons->content.name, persons->content.firstname, persons->content.age);
        i++;
    }
    pause_for_input();
}

void clear_persons(ListElement *persons)
{
    (void)printf("--- [Clearing] ---\n");

    ListElement *start = persons;
    while(persons->next != start) {
        ListElement *temp;
        temp = persons->next;
        persons->next = temp->next;
        free(temp);
    }
    (void)printf("Successfully cleared list!\n\n");

    pause_for_input();
}

void pause_for_input(void)
{
    stdflush();
    (void)printf("Press any key to continue...");
    getchar();
}

int personcmp(const Person a, const Person b)
{
    int name_compare = strcmp(a.name, b.name);
    if(name_compare != 0) {
        return name_compare;
    }
    int firstname_compare = strcmp(a.firstname, b.firstname);
    if(firstname_compare != 0) {
        return firstname_compare;
    }
    return a.age == b.age ? 0 : (a.age > b.age ? 1 : -1);
}

void stdflush(void)
{
    do {} while(getchar() != '\n');
}
