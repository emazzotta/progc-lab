/* Header file main.h */
#ifndef MAIN_H
#define MAIN_H

typedef struct {
  char name[20];
  char firstname[20];
  unsigned age;
} Person;

typedef struct LE ListElement;

struct LE {
  Person content;
  ListElement *next;
};

void insert_person(ListElement*);
void remove_person(ListElement*);
void show_persons(ListElement*);
void clear_persons(ListElement*);
void pause_for_input(void);
int personcmp(const Person, const Person);
void stdflush(void);

#endif
