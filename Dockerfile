FROM ubuntu

RUN apt-get -qqy update && apt-get install -y vim build-essential gcc gdb libtool automake doxygen

RUN mkdir -p /usr/src/progc/
WORKDIR /usr/src/progc/
ENV WORKDIR /usr/src/progc/
ADD . /usr/src/progc/
ADD .bashrc /root/

CMD ["./run"]