/* ----------------------------------------------------------------------------
 * --  _____       ______  _____                                              -
 * -- |_   _|     |  ____|/ ____|                                             -
 * --   | |  _ __ | |__  | (___    Institute of Embedded Systems              -
 * --   | | | '_ \|  __|  \___ \   Zuercher Hochschule Winterthur             -
 * --  _| |_| | | | |____ ____) |  (University of Applied Sciences)           -
 * -- |_____|_| |_|______|_____/   8401 Winterthur, Switzerland               -
 * ----------------------------------------------------------------------------
 */
/**
 * @file
 * @brief Lab implementation
 */
#include <stdio.h>
#include <stdlib.h>
#include "date_calculator.h"


int main(void) 
{
    Date date;
    scanf("%d%d%d", &date.day, &date.month, &date.year);
    if(date.year >= 1583 && !is_valid_date(date)) {
        (void)printf("Sorry, but you entered an invalid date!\n");
        return EXIT_FAILURE;
    }
    Date next_date = calculate_next_day(date);
	(void)printf("Next is: %d.%d.%d\n", next_date.day, next_date.month, next_date.year);
	return EXIT_SUCCESS;
}

Date calculate_next_day(Date current_date)
{
    Date next_date = {
        current_date.day,
        current_date.month,
        current_date.year,
    };

    if(current_date.day == 31 && current_date.month == Dec) {
        next_date.year += 1;
        next_date.day = 1;
        next_date.month = 1;
    } else if(current_date.day >= 28) {
        if(days_of_month(current_date) == current_date.day) {
            next_date.day = 1;
            next_date.month += 1;
        } else {
            next_date.day += 1;
        }
    } else {
        next_date.day += 1;
    }

    return next_date;
}

int days_of_month(Date date)
{
    int number_of_days;

    if (date.month == Apr || date.month == Jun || date.month == Sep || date.month == Nov) {
        number_of_days = 30;
    } else if (date.month == Feb) {
        if (is_leap_year(date.year) == TRUE) {
            number_of_days = 29;
        } else {
            number_of_days = 28;
        }
    } else {
        number_of_days = 31;
    }

    return number_of_days;
}

int is_valid_date(Date date)
{
    return !(date.day <= 0 ||
             date.day > days_of_month(date) ||
             date.month < Jan ||
             date.month > Dec);
}

int is_leap_year(int year)
{
    return (year % 4 == 0 && year % 100 != 0) || (year % 400 == 0) ? TRUE : FALSE;
}