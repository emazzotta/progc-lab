/* Header file main.h */
#ifndef MAIN_H
#define MAIN_H

#define TRUE 1
#define FALSE -1

typedef enum {Jan=1, Feb, Mar, Apr, May, Jun, Jul, Aug, Sep, Oct, Nov, Dec} months;

typedef struct {
  int day;
  int month;
  int year;
} Date;

Date calculate_next_day(Date);
int days_of_month(Date);
int is_valid_date(Date date);
int is_leap_year(int);

#endif
