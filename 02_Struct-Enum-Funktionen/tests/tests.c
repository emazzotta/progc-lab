/* ----------------------------------------------------------------------------
 * --  _____       ______  _____                                              -
 * -- |_   _|     |  ____|/ ____|                                             -
 * --   | |  _ __ | |__  | (___    Institute of Embedded Systems              -
 * --   | | | '_ \|  __|  \___ \   Zuercher Hochschule Winterthur             -
 * --  _| |_| | | | |____ ____) |  (University of Applied Sciences)           -
 * -- |_____|_| |_|______|_____/   8401 Winterthur, Switzerland               -
 * ----------------------------------------------------------------------------
 */
/**
 * @file
 * @brief Test suite for the given package.
 */
#include <stdio.h>
#include <stdlib.h>
#include "CUnit/Basic.h"
#include "test_utils.h"

#ifndef TARGET // must be given by the make file --> see test target
#error missing TARGET define
#endif

/// Epsilon for double comparisons. cf: weil ein double nicht gleich ein anderer double im Nachkommabereich
#define EPSILON 0.1
/// @brief The name of the STDOUT text file.
#define OUTFILE "stdout.txt"
/// @brief The name of the STDERR text file.
#define ERRFILE "stderr.txt"
/// @brief The stimulus for next day calculation
#define INFILE_VALID "valid-date.test_txt"
/// @brief The stimulus for leap year calculation
#define INFILE_LEAP "leap.test_txt"
/// @brief The stimulus for no leap year calculation
#define INFILE_NO_LEAP "no-leap.test_txt"
/// @brief The stimulus for year overflow
#define INFILE_YEAR_OVERFLOW "year-overflow.test_txt"
/// @brief The stimulus for month overflow
#define INFILE_MONTH_OVERFLOW "month-overflow.test_txt"
/// @brief The stimulus for invalid day
#define INFILE_INVALID_DAY "invalid-day.test_txt"
/// @brief The stimulus for invalid month
#define INFILE_INVALID_MONTH "invalid-month.test_txt"
/// @brief The stimulus for invalid month
#define INFILE_INVALID_YEAR "invalid-year.test_txt"

// setup & cleanup
static int setup(void)
{
    remove_file_if_exists(OUTFILE);
	remove_file_if_exists(ERRFILE);
	return 0; // success
}
static int teardown(void)
{
	// Do nothing.
	return 0; // success
}

// tests
static void test_day_after_valid_date(void)
{
	// arrange
	const char *out_txt[] = {
        "Next is: 3.2.2004\n"
	};
	// act
	int exit_code = system(XSTR(TARGET) " 1>" OUTFILE " 2>" ERRFILE " <" INFILE_VALID);
	// assert
	CU_ASSERT_EQUAL(exit_code, 0);
	assert_lines(OUTFILE, out_txt, sizeof(out_txt)/sizeof(*out_txt));
}

// tests
static void test_leap_year(void)
{
	// arrange
	const char *out_txt[] = {
        "Next is: 29.2.2012\n"
    };
	// act
	int exit_code = system(XSTR(TARGET) " 1>" OUTFILE " 2>" ERRFILE " <" INFILE_LEAP);
	// assert
	CU_ASSERT_EQUAL(exit_code, 0);
	assert_lines(OUTFILE, out_txt, sizeof(out_txt)/sizeof(*out_txt));
}

// tests
static void test_no_leap_year(void)
{
	// arrange
	const char *out_txt[] = {
        "Next is: 1.3.2014\n"
    };
	// act
	int exit_code = system(XSTR(TARGET) " 1>" OUTFILE " 2>" ERRFILE " <" INFILE_NO_LEAP);
	// assert
	CU_ASSERT_EQUAL(exit_code, 0);
	assert_lines(OUTFILE, out_txt, sizeof(out_txt)/sizeof(*out_txt));
}

// tests
static void test_year_overflow(void)
{
	// arrange
	const char *out_txt[] = {
        "Next is: 1.1.2005\n"
    };
	// act
	int exit_code = system(XSTR(TARGET) " 1>" OUTFILE " 2>" ERRFILE " <" INFILE_YEAR_OVERFLOW);
	// assert
	CU_ASSERT_EQUAL(exit_code, 0);
	assert_lines(OUTFILE, out_txt, sizeof(out_txt)/sizeof(*out_txt));
}

// tests
static void test_month_overflow(void)
{
	// arrange
	const char *out_txt[] = {
        "Next is: 1.12.2005\n"
    };
	// act
	int exit_code = system(XSTR(TARGET) " 1>" OUTFILE " 2>" ERRFILE " <" INFILE_MONTH_OVERFLOW);
	// assert
	CU_ASSERT_EQUAL(exit_code, 0);
	assert_lines(OUTFILE, out_txt, sizeof(out_txt)/sizeof(*out_txt));
}

// tests
static void test_invalid_year(void)
{
	// arrange
	const char *out_txt[] = {
        "Next is: 1.15.1582\n"
    };
	// act
	int exit_code = system(XSTR(TARGET) " 1>" OUTFILE " 2>" ERRFILE " <" INFILE_INVALID_YEAR);
	// assert
	CU_ASSERT_EQUAL(exit_code, 0);
	assert_lines(OUTFILE, out_txt, sizeof(out_txt)/sizeof(*out_txt));
}

// tests
static void test_invalid_month(void)
{
	// arrange
	const char *out_txt[] = {
        "Sorry, but you entered an invalid date!\n"
    };
	// act
	int exit_code = system(XSTR(TARGET) " 1>" OUTFILE " 2>" ERRFILE " <" INFILE_INVALID_MONTH);
	// assert
	CU_ASSERT_EQUAL(exit_code, 256);
	assert_lines(OUTFILE, out_txt, sizeof(out_txt)/sizeof(*out_txt));
}

// tests
static void test_invalid_day(void)
{
	// arrange
	const char *out_txt[] = {
        "Sorry, but you entered an invalid date!\n"
    };
	// act
	int exit_code = system(XSTR(TARGET) " 1>" OUTFILE " 2>" ERRFILE " <" INFILE_INVALID_DAY);
	// assert
	CU_ASSERT_EQUAL(exit_code, 256);
	assert_lines(OUTFILE, out_txt, sizeof(out_txt)/sizeof(*out_txt));
}

/**
 * @brief Registers and runs the tests.
 */
int main(void)
{
	// setup, run, teardown
	TestMainBasic("Date Calculation", setup, teardown
				  , test_day_after_valid_date
				  , test_leap_year
				  , test_no_leap_year
				  , test_year_overflow
				  , test_month_overflow
				  , test_invalid_year
				  , test_invalid_month
				  , test_invalid_day
				  );
}
