/* Header file main.h */
#ifndef MAIN_H
#define MAIN_H

#define TRUE 0
#define FALSE -1

int cmpstr(const void* a, const void* b);
int word_exists(char *[], size_t, char *);

#endif
