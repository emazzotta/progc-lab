/* ----------------------------------------------------------------------------
 * --  _____       ______  _____                                              -
 * -- |_   _|     |  ____|/ ____|                                             -
 * --   | |  _ __ | |__  | (___    Institute of Embedded Systems              -
 * --   | | | '_ \|  __|  \___ \   Zuercher Hochschule Winterthur             -
 * --  _| |_| | | | |____ ____) |  (University of Applied Sciences)           -
 * -- |_____|_| |_|______|_____/   8401 Winterthur, Switzerland               -
 * ----------------------------------------------------------------------------
 */
/**
 * @file
 * @brief Lab implementation
 */
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include "sorter.h"

int main(void) 
{
	char *word_list[100];
    char word[20];
    int word_list_len = 0;

    for(int i = 0; i < 100; i++) {
        (void)printf("Enter the word: ");
        scanf("%s", word);

        if(strcmp(word, "ZZZ") == TRUE) {
            break;
        }

        if(word_exists(word_list, i, word) == TRUE) {
            i--;
        } else {
            word_list_len = i + 1;
            word_list[i] = strdup(word);
        }
	}

    qsort(word_list, word_list_len, sizeof(char*), cmpstr);

    for(int i = 0; i < word_list_len; i++) {
        (void)printf("%d: %s\n", i, word_list[i]);
    }

	return EXIT_SUCCESS;
}

int cmpstr(const void* a, const void* b)
{
    const char* aa = *(const char**)a;
    const char* bb = *(const char**)b;
    return strcmp(aa,bb);
}

int word_exists(char *word_list[], size_t len, char *word)
{
    int word_exists = FALSE;
    for(int i = 0; i < len; i++) {
        if(strcmp(word_list[i], word) == TRUE) {
            word_exists = TRUE;
            break;
        }
    }
    return word_exists;
}
