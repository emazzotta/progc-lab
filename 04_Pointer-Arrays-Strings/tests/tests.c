/* ----------------------------------------------------------------------------
 * --  _____       ______  _____                                              -
 * -- |_   _|     |  ____|/ ____|                                             -
 * --   | |  _ __ | |__  | (___    Institute of Embedded Systems              -
 * --   | | | '_ \|  __|  \___ \   Zuercher Hochschule Winterthur             -
 * --  _| |_| | | | |____ ____) |  (University of Applied Sciences)           -
 * -- |_____|_| |_|______|_____/   8401 Winterthur, Switzerland               -
 * ----------------------------------------------------------------------------
 */
/**
 * @file
 * @brief Test suite for the given package.
 */
#include <stdio.h>
#include <stdlib.h>
#include "CUnit/Basic.h"
#include "test_utils.h"
#include "sorter.h"

#ifndef TARGET
#error missing TARGET define
#endif

/// @brief The name of the STDOUT text file.
#define OUTFILE "stdout.txt"
/// @brief The name of the STDERR text file.
#define ERRFILE "stderr.txt"
/// @brief The name of the test input file.
#define INFILE "input.test_txt"

// setup & cleanup
static int setup(void)
{
	remove_file_if_exists(OUTFILE);
	remove_file_if_exists(ERRFILE);
	return 0; // success
}

static int teardown(void)
{
	return 0; // success
}


// tests
static void test_that_jagged_array_sorting_works_as_expected(void)
{
	// arrange
	const char *out_txt[] = {
		"Enter the word: ",
		"Enter the word: ",
		"Enter the word: ",
		"Enter the word: ",
		"Enter the word: ",
		"Enter the word: ",
		"Enter the word: ",
		"Enter the word: ",
		"Enter the word: ",
		"Enter the word: ",
		"Enter the word: ",
		"Enter the word: ",
        "0: AA\n",
        "1: BB\n",
        "2: CCCC\n",
        "3: Oscar\n",
        "4: aaaa\n",
        "5: auto\n",
        "6: bbbbb\n",
        "7: hotel\n",
        "8: papa\n",
        "9: rad\n",
        "10: zulu\n"
	};
	// act
	int exit_code = system(XSTR(TARGET) " 1>" OUTFILE " 2>" ERRFILE " <" INFILE);
	// assert
	CU_ASSERT_EQUAL(exit_code, 0);
	assert_lines(OUTFILE, out_txt, sizeof(out_txt)/sizeof(*out_txt));
}


/**
 * @brief Registers and runs the tests.
 */
int main(void)
{
	// setup, run, teardown
	TestMainBasic("Word sort", setup, teardown
				  , test_that_jagged_array_sorting_works_as_expected
				  );
}